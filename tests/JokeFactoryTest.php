<?php

namespace Gasperoid\ChuckJokes\Tests;

use Gasperoid\ChuckJokes\JokeFactory;
use PHPUnit\Framework\TestCase;

class JokeFactoryTest extends TestCase
{
    /**
     * @test
     */
    public function it_returns_a_random_joke()
    {
        $jokes = new JokeFactory([
            'This is a joke'
        ]);
        $joke = $jokes->getRandomJoke();

        $this->assertSame('This is a joke', $joke);
    }

    /**
     * @test
     */
    public function it_returns_a_predefined_joke()
    {
        $chuckJokes = [
            'If you can see Chuck Norris, he can see you. If you can\'t see Chuck Norris you may be only seconds away from death',
            'Chuck Norris counted to infinity... Twice.',
            'Chuck Norris does not wear a condom. Because there is no such thing as protection from Chuck Norris.',
            'Chuck Norris\' tears cure cancer. Too bad he has never cried.'
        ];

        $jokes = new JokeFactory($chuckJokes);
        $joke = $jokes->getRandomJoke();

        $this->assertContains($joke, $chuckJokes);
    }
}