<?php

namespace Gasperoid\ChuckJokes;

class JokeFactory
{
    protected array $jokes = [
        'If you can see Chuck Norris, he can see you. If you can\'t see Chuck Norris you may be only seconds away from death',
        'Chuck Norris counted to infinity... Twice.',
        'Chuck Norris does not wear a condom. Because there is no such thing as protection from Chuck Norris.',
        'Chuck Norris\' tears cure cancer. Too bad he has never cried.'
    ] ;

    public function __construct(?array $jokes = null)
    {
        if ($jokes) {
            $this->jokes = $jokes;
        }
    }

    public function getRandomJoke(): string {
        return $this->jokes[array_rand($this->jokes)];
    }
}